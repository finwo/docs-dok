<?php

// Include composer
$loader = include __DIR__ . '/../vendor/autoload.php';

// Fetch path
$path = @array_shift(explode('?', $_SERVER['REQUEST_URI']));

// Keep it simple
function not_found() {
    header('HTTP/1.0 404 Not Found');
    die('404');
}

// Some security
if (strpos($path, '..') !== false) {
    not_found();
}

// Some options to append to the path later on
$searchOptions = array(
    '',
    '.md',
    '/index.md',
);

function flatten_vars($vars, $path = array(), $depth = 0) {
    $output = array();
    foreach($vars as $key => $value) {
        switch(gettype($value)) {
            case 'array':
            case 'object':

                // Lets iterate down then
                $output = array_merge($output, flatten_vars($value, array_merge($path, array(
                    $key
                )), $depth+1));

                break;
            default:

                // Add to the output
                $k = trim(implode(':', array_merge($path, array(
                    strtolower($key)
                ))), ':');

                $output[$k] = $value;
                break;
        }
    }

    // If we're the root, add surrounding :: marks
    if ($depth == 0) {
        $tmp = $output;
        $output = array();
        foreach($tmp as $k=>$v) {
            $output["::$k::"] = $v;
        }
    }

    // Return the flattened array
    return $output;
}

// Check if one of the files exists
// Take the data folder as root
foreach($searchOptions as $append) {
    $fn = realpath(__DIR__.'/../data/'.$path.$append);
    if (!!$fn && !is_dir($fn) ) {

        // Start rendering stuff
        $renderer = new Parsedown();

        // Variables
        $vars = array(
            'server'  => $_SERVER,
            'request' => $_REQUEST,
        );

        // Render body etc
        $vars['body'] = $renderer->text(file_get_contents($fn));
        $vars['path'] = substr($fn, strlen(realpath(__DIR__.'/../data')));

        // Replace tags by values
        ob_start(function($buffer) use ($vars) {
            $vars = flatten_vars($vars);
            $buffer = str_replace(array_keys($vars), array_values($vars), $buffer);
            return $buffer;
        });

        // Use a template
        include __DIR__ . '/../data/template.php';
        exit;
        
    }
}

not_found();
