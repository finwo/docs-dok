Dokku-alt documentation
-----------------------

/ [index](index.md)

----

#### What is [dokku-alt](https://github.com/dokku-alt/dokku-alt)?

Basically it's dokku on steroids. It's a mini heroku-like deployment system for a single server.

----

Remote link to this documentation: https://github.com/finwo/docs-dok/blob/master/data/index.md

This documentation contains everything you need to get started using dokku-alt within the VpsCash environment (or almost anywhere you'd like).

----

If you're a new developer on a team using a dokku server, you'll only need the [client](client/index.md) bits of this documentation. A quick start can be made by jumping directly to [client/usage](client/usage/index.md) if you only need to deploy code.

----

#### [Server](server/index.md)

* [Installation](server/installation.md)
* [Setup](server/setup.md)
* [Management](server/management/index.md)
    * [Applications](server/management/applications.md)
    * [Users](server/management/users.md)

----

#### [Client](client/index.md)

* [Installation](client/installation.md)
* [Usage](client/usage/index.md)
    * [Command list](client/usage/cmdlist.md)
    * [Deploying code](client/usage/deploying.md)
    * [Managing projects](client/usage/managing.md)

----

#### Changelog

- 2016-07-05
    - Added explanation for new developers
    - Added explanation for user management
- 2016-07-04
    - Project init

----

#### The MIT License (MIT)
 
Copyright (c) 2016 Virtual PC Services B.V.

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
