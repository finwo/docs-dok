Client installation
-------------------

/ [index](../index.md) / [client](index.md) / [installation](installation.md)

----

**The CLI client for dokku-alt will only work if you're granted admin permissions.**

If you're just going to use a dokku-alt server without using admin functionality, please see [usage](usage.md).

----

The following command lines have to be available to be able to set up a working dokku-alt client:

- git
- ssh
- bash or zsh

Installation manual for some common linux distributions:

#### Debian / Ubuntu (based)

**ssh and git**
```
sudo apt-get install ssh git
```

 **bash**
```
sudo apt-get install bash
```

 **zsh**
```
sudo apt-get install zsh
```

----

Configuration needs to be done on a per-project basis.
The global configuration exists to take some pain out of the process

----

#### Creating a command to use

These only need to be done once per machine you're working on, assuming you don't overwrite the created settings.

1.  If you're going for admin permissions, send your public ssh key (run `cat $HOME/.ssh/id_rsa.pub`) to your system administrator & ask for access. You can run `ssh-keygen` if you haven't got a key already.

2.  Make sure your $HOME/bin folder is within your $PATH variable. You can do this by including the following code in your `.bashrc` or `.zshrc` file. 

    ```
    export PATH=$HOME/bin:$PATH
    ```

3.  Add an SSH alias for the host (replace the domain and port for your own)

    ```
    export DOKKU_DOMAIN=example.com
    export DOKKU_PORT=22
    mkdir -p $HOME/.ssh
    touch $HOME/.ssh/config
    echo -e "Host dokku\n   HostName $DOKKU_DOMAIN\n   User dokku\n   Port $DOKKU_PORT" >> $HOME/.ssh/config
    ```

4.  Add an executable `dokku` file within your $HOME/bin folder

    ```
    mkdir -p $HOME/bin
    echo -e '#!/bin/bash\nssh dokku@dokku "$@"' > $HOME/bin/dokku
    chmod +x $HOME/bin/dokku
    ```

5.  If your system administrator has granted you admin permissions, you can check if the configuration works by spawning a new shell (or reloading your configuration through `source ~/.bashrc` or `source ~/.zshrc`) and running `dokku help`. Accept the host key & you should see a list of available commands.
