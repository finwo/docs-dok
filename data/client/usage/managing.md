Managing projects
-----------------

/ [index](../../index.md) / [client](../index.md) / [usage](index.md) / [managing](managing.md)

----

**Make sure you've completed the [installation](../installation.md) steps first**

----

**Admin access on the server is required for this**

1.  Open a shell in the root directory of your local repository

2.  Make sure the git repository is initialized, or initialize it on the spot using `git init`

3.  run `dokku create <project-name>` to add the project to the server

4.  Add the remote to git with by running `git add remote dokku dokku@dokku:<project-name>`

----

#### Showing the current project domains

**Admin access on the server is required for this**

1.  Open a new shell (or re-use an existing one) 

2.  Run `dokku domains:get <project-name>` to show the current list of domains the project replies to.

----

#### Setting domains for a project

**Admin access on the server is required for this**

**Settings the domains will completely overwrite the domain list, so you'll need to append the current domains if you want to add one.**

1.  Open a new shell (or re-use an existing one) 

2.  (optional) Show the current project's domain list, to be able to append that later on

3.  Run `dokku domains:set <project-name> <domain> [<another domain>] [...]`
