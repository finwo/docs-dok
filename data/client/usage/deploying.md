Deploying code
--------------

/ [index](../../index.md) / [client](../index.md) / [usage](index.md) / [deploying](deploying.md)

----

#### Joining an existing project

1.  Make sure you have a public ssh key (most likely at `$HOME/.ssh/id_rsa.pub`), and have `git` installed

2.  Ask for deploy access to the repository from your server administrator (he'll need your public SSH key)

3.  Open a shell in the root directory of your local repository

4.  Add the remote to git with by running `git add remote dokku dokku@dokku:<projectname>`

----

#### Deploying your code

1.  Make sure you have deploy access on the project (you can ask your server administrator to verify this)

2.  Verify you have the dokku remote added to your local repository by running `git remote -v` 

3.  Be **very** certain you want to deploy your code to the server

4.  Check which branch you're on by running `git branch`. The current one will be marked in the view

5.  Run `git push dokku <current-branch-name>:master` to push your code to the server

6.  Write down the previous commit hash displayed at the end of the deploy in case all hell breaks loose.
    ```
    To dokku@dokku:<application-name>
       <previous-hash>..<current-hash>  master -> master

    ```

----

#### Rolling back to previous version

1.  Make sure you have deploy access on the project (you can ask your server administrator to verify this)

2.  Verify you have the dokku remote added to your local repository by running `git remote -v` 

3.  Be **very** certain you want to roll back the project

4.  Roll back your local repository to the correct commit using `git checkout <commit-hash>`. Your local repository should now be in `DETACHED HEAD` mode.

5.  Check again if you want to push this code to the server

6.  Run `git push -f dokku HEAD:master` or `git push -f dokku <commit-hash>:master` to push the code to the server.
