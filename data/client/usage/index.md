Client usage
------------

/ [index](../../index.md) / [client](../index.md) / [usage](index.md)

----

Deploying code requires less permissions than managing projects. Therefore the subjects have been seperated.

- [Command list](cmdlist.md)
- [Deploying code](deploying.md)
- [Managing projects](managing.md)
