User management
---------------

/ [index](../../index.md) / [server](../index.md) / [management](index.md) / [users](users.md)

----

## Managing developers

#### List users with deploy permissions for an application

You'll need a functioning dokku-alt client.

Run `dokku deploy:list <app>` from your terminal. This command will display users & their fingerprints which are allowed to deploy to an application.

----

#### Allow a new user to deploy

You'll need a functioning dokku-alt client and the public SSH key of the user you're about to add.

Run `dokku deploy:allow <app>` from your terminal. This command will listen for a public SSH key on it's standard input, so you can copy-paste the key into the terminal as well. End the command with an `Enter` and `Ctrl+D`.

----

#### Revoke a deploy permissions from a user

You'll need a functioning dokku-alt client.

Run `dokku deploy:revoke <app> <fingerprint>` from your terminal. You can fetch the fingerprints from displaying the users with deploy permissions.

----

## Managing administrators

#### Display admin user list

You'll need a functioning dokku-alt client.

Run `dokku access:list` to display a list of admin users & their fingerprints. The names might be confusing though, as they're probably all called 'admin'

----

#### Getting information about an admin

You'll need a functioning dokku-alt client.

Run `dokku access:info <fingerprint>` to display an admin's line for the `authorized_keys` file, including it's public SSH key. You can fetch the fingerprints from displaying the admin user list.

----

#### Adding an admin user

You'll need a functioning dokku-alt client and the public SSH key of the user you're about to add.

Run `dokku access:add` from your terminal. This command will listen for a public SSH key on it's standard input, so you can copy-paste the key into the terminal as well. End the command with an `Enter` and `Ctrl+D`.

----

#### Removing an admin user

You'll need a functioning dokku-alt client.

Run `dokku access:remove <fingerprint>` from your terminal. You can fetch the fingerprints from displaying the admin user list.
