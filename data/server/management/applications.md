Application management
----------------------

/ [index](../../index.md) / [server](../index.md) / [management](index.md) / [applications](applications.md)

----

#### Show all applications

You'll need a functioning dokku-alt client.

Run `dokku list` or `dokku apps` or `dokku apps:list` from your terminal. These will show you a list of application names currently residing on the server.

----

#### Show application log

You'll need a functioning dokku-alt client.

Run `dokku logs <app> [-t] [-f]` from your terminal. Adding `-f` to the command keeps your terminal linked to the logs, just like `tail -f` would.

----

#### Show config variables

You'll need a functioning dokku-alt client.

Run `dokku config <app>` to display a list of configuration variables. These variables will show up in the environment of the application. (for PHP, they'd be in `$_ENV`) 

----

#### Show specific config variable

You'll need a functioning dokku-alt client.

Run `dokku config:get <app> KEY` from your terminal. This will display the value of the key you've requested.

----

#### Add / set configuration variable

You'll need a functioning dokku-alt client.

Run `dokku config:set <app> KEY1=VALUE1 [KEY2=VALUE2 ...]` from your terminal. The variables you've set will show up in the environment of the application. (for PHP, they'd be in `$_ENV`) 

----

#### Remove configuration variable

You'll need a functioning dokku-alt client.

Run `dokku config:unset <app> KEY1 [KEY2 ...]` from your terminal. This will remove the configuration variables you've set for an application.

----

#### Create an new application

You'll need a functioning dokku-alt client (or admin permissions).

Run `dokku create <app>` from your terminal. This will initialize an empty application on the server. As admin, you can also push to the server on a non-existing application to create one under that name.

Creating an empty application is useful if you want to prepare deploy permissions before actually deploying the application.

----

#### Delete an application

You'll need a functioning dokku-alt client.

Run `dokku delete <app>` from your terminal. Be **very** sure you want to do this. It will delete the application and all it's history.

<!--
```
disable <app>                                   Disable specific app
domains:get <app>                               Get domains for an app
domains:redirect:get <app>                      Get redirect domains for an app
domains:redirect:set <app> <domains...>         Set redirect app domains
domains:set <app> <domains...>                  Set app domains
enable <app>                                    Re-enable specific app
enter <app>                                     Enter into currently running container
exec <app> <cmd>                                Execute command in currently running container
htpasswd:add <app> <user>                       Add http-basic auth user
htpasswd:disable <app>                          Remove http-basic Auth
htpasswd:remove <app> <user>                    Remove user
rebuild:all:force                               Remove all caches and rebuild all apps
rebuild:all                                     Rebuild all apps
rebuild <app> [ref]                             Rebuild an app (using optional ref)
rebuild:force <app> [ref]                       Remove all caches and rebuild an app (using optional ref)
restart <app>                                   Restart specific app (not-redeploy)
run <app> <cmd>                                 Run a command in the environment of an application
start <app>                                     Stop specific app
status <app>                                    Status of specific app
stop <app>                                      Stop specific app
tag:add <app> <tag>                             Tag latest running image using specified name
tag:list <app>                                  List all image tags
tag:rm <app> <tag>                              Tag latest running image using specified name
```
-->
