Server management
-----------------

/ [index](../../index.md) / [server](../index.md) / [management](index.md)

----

As there are a load of management options, the have been split by category.

This page is still a work in progress

----

- [Application management](applications.md)
- [User management](users.md)

<!--
- [Backups](backups.md)
    **TODO**
    ```
    backup:export [file]                            Export dokku configuration files
    backup:import [file]                            Import dokku configuration files
    ```

- [Debugging](debugging.md)
    **TODO**
    ```
    backup:export [file]                            Export dokku configuration files
    backup:import [file]                            Import dokku configuration files
    ```

- [Web manager](webmanager.md)
    **TODO**
    ```
    manager:disable                                 Disable dokku-alt-manager application
    manager:enable                                  Enable dokku-alt-manager application
    manager:install [revision]                      Install and configure dokku-alt-manager
    manager:uninstall                               Uninstall and wipe dokku-alt-manager
    manager:upgrade [revision]                      Disable dokku-alt-manager application
    ```

- [Databases](db/index.md)
    **TODO**
    - MariaDB
        **TODO**
        ```
        mariadb:console:admin                           Launch admin (be careful!)
        mariadb:console <app> <db>                      Launch console for MariaDB container
        mariadb:create <db>                             Create a MariaDB database
        mariadb:delete <db>                             Delete specified MariaDB database
        mariadb:dump <app> <db>                         Dump database for an app
        mariadb:info <app> <db>                         Display application informations
        mariadb:link <app> <db>                         Link database to app
        mariadb:list <app>                              List linked databases
        mariadb:restart                                 Restart MariaDB container (for example to switch image)
        mariadb:unlink <app> <db>                       Unlink database from app
        ```
        
    - [MongoDB](db/mongo.md)
        **TODO**
        ```
        mongodb:console <app> <db>                      Launch console for MongoDB container
        mongodb:create <db>                             Create a MongoDB database
        mongodb:delete <db>                             Delete specified MongoDB database
        mongodb:dump <app> <db> <collection>            Dump database collection in bson for an app
        mongodb:export <app> <db> <collection>          Export database collection for an app
        mongodb:import <app> <db> <collection>          Import database collection for an app
        mongodb:info <app> <db>                         Display application informations
        mongodb:link <app> <db>                         Link database to app
        mongodb:list <app>                              List linked databases
        mongodb:restart                                 Restart MongoDB container (for example to switch image)
        mongodb:unlink <app> <db>                       Unlink database from app
        ```
    - [PostgreSQL](db/postgres.md)
        **TODO**
        ```
        postgresql:console:admin                        Launch admin console (be careful!)
        postgresql:console <app> <db>                   Launch console for PostgreSQL container
        postgresql:create <db>                          Create a PostgreSQL database
        postgresql:delete <db>                          Delete specified PostgreSQL database
        postgresql:dump <app> <db>                      Dump database for an app
        postgresql:info <app> <db>                      Display application informations
        postgresql:link <app> <db>                      Link database to app
        postgresql:list <app>                           List linked databases
        postgresql:restart                              Restart PostgreSQL container (for example to switch image)
        postgresql:unlink <app> <db>                    Unlink database from app
        ```
    - [Redis](db/redis.md)
        **TODO**
        ```
        redis:create <app>                              Create a Redis database
        redis:delete <app>                              Delete specified Redis database
        redis:info <app>                                Display application information
        ```

- [Plugins](plugins.md)
    **TODO**
    ```
    plugins-install                                 Install active plugins
    plugins                                         Print active plugins
    plugins-update                                  Update active plugins
    ```

- [Persistent storage](volumes.md)
    **TODO**
    ```
    volume:create <name> <paths...>                 Create a data volume for specified paths
    volume:delete <name>                            Delete a data volume
    volume:info <name>                              Display volume information
    volume:link <app> <name>                        Link volume to app
    volume:list:apps <name>                         Display apps linked to volume
    volume:list                                     List volumes
    volume:unlink <app> <name>                      Unlink volume from app
    ```

#### No category yet

```
help                                            Print the list of commands
nginx:import-ssl <app>                          Imports a tarball from stdin; should contain server.crt and server.key
preboot:cooldown:time <app> <secs>              Re-enable specific app
preboot:disable <app>                           Stop specific app
preboot:enable <app>                            Enable specific app
preboot:status <app>                            Status of specific app
preboot:wait:time <app> <secs>                  Restart specific app (not-redeploy)
ssl:certificate <app>                           Pipe signed certifcate with all intermediates for an APP
ssl:forget <app>                                Wipes certificate for an APP
ssl:generate <app>                              Generate certificate signing request for an APP
ssl:info <app>                                  Show info about certifcate and certificate request
ssl:key <app>                                   Pipe private key for an APP
ssl:selfsigned <app>                            Generate self-signed certificate an APP
top <app> [args...]                             Show running processes
url <app>                                       Show the URL for an application
version                                         Print dokku's version
```
-->
