Server installation
-------------------

/ [index](../index.md) / [server](index.md) / [installation](installation.md)

----

#### Requirements

- An ubuntu server (preferably 14.04) with root access. ([mini](http://archive.ubuntu.com/ubuntu/dists/trusty-updates/main/installer-amd64/current/images/netboot/mini.iso) or [server](http://nl.archive.ubuntu.com/ubuntu-cdimages/14.04/release/ubuntu-14.04.4-server-amd64+mac.iso))
- Root access to that ubuntu server
- Access to port 2000 with http on the server
- Copy-paste skills

----

#### Installing dokku-alt itself

Installing dokku-alt has been made easy, as described [here](https://github.com/dokku-alt/dokku-alt#installing). Just run the following code on the server:

```
sudo bash -c "$(curl -fsSL https://raw.githubusercontent.com/dokku-alt/dokku-alt/master/bootstrap.sh)"
```

At the end if the installation, it will start a web gui on port 2000 for entering the first admin key, so you'll need to figure that out on your own.
Alternatively, you can walk through the following steps once the gui server is started:

1.  Press `Ctrl+C` to stop the running webserver

2.  Look up your own public SSH key for remote access (often located at `$HOME/.ssh/id_rsa.pub`)

3.  Run `dokku access:add` as root on your freshly installed server. You will see no output in your console.

4.  Copy-paste your public key into the console, press `Enter` and `Ctrl+D`

5.  Your public key has now been added to the server as admin
